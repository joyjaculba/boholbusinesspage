<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Console\GeneratorCommand;

class sepirothCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sepiroth:build {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This build a scaffolding for grid,form,and controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* creating the model */
        $name = $this->parseName($this->getNameInput());

        $path = $this->getPath($name);
        $model_exist = false;
        $model_form_exist = false;
        $model_grid_exist = false;
        //$this->info($this->getDefaultNamespace($this->laravel->getNamespace()));
       
            $name = $this->parseName($this->getNameInput().'/'.$this->getNameInput());
            $namespace =  $this->parseName($this->getNameInput());
            
            $this->makeDirectory($this->getPath($name));        
            
            if (!$this->files->exists($this->getPath($name))) {
                $this->info('creating ' . $namespace . ' on path : ' . $this->getPath($name));
                $this->files->put($this->getPath($name), $this->doBuildClass($namespace,''));
            }else{
                $this->error($this->getPath($name).' already exists!');
            }

            if (!$this->files->exists($this->getPath($name.'Grid'))) {  
                $this->info('creating ' . $namespace . ' on path : ' . $this->getPath($name.'Grid'));
                $this->files->put($this->getPath($name.'Grid'), $this->doBuildClass($namespace,'grid'));
            }else{
                $this->error($this->getPath($name.'Grid').' already exists!');
            }

            if (!$this->files->exists($this->getPath($name.'Form'))) { 
                $this->info('creating ' . $namespace . ' on path : ' . $this->getPath($name.'Form'));
                $this->files->put($this->getPath($name.'Form'), $this->doBuildClass($name.'Form','form'));
            }else{
                $this->error($this->getPath($name.'Form').' already exists!');
            }
            //return;

            $controllernamespace = "App\Http\Controllers";
            $controllername = $this->getNameInput();
            $controllerpath = $this->laravel['path'] . "/Http/Controllers/" . $controllername;
            
            $this->info($controllernamespace);
            $this->info($controllername);
            $this->info($controllerpath);
            if (!$this->files->exists($controllerpath)) { 
                $this->info('creating ' . $controllername . ' on path : ' . $controllerpath);

                $model = $this->getNameInput();
                $gridname = $this->getNameInput().'Grid';
                $formname = $this->getNameInput().'Form';

                $stub = $this->files->get($this->getControllerStub());
                $stub = $this->replaceDummyModel($stub, $model);
                $stub = $this->replaceDummyForm($stub,$formname);
                $stub = $this->replaceDummyGrid($stub,$gridname);
                $stub = $this->replaceDummyRoute($stub, strtolower($model));

                $this->files->put($controllerpath.'Controller.php', $stub);
            }else{
                $this->error($controllerpath.' already exists!');
            }
            //$this->info($name.' created successfully.');
        
    }
    
    protected function getStub()
    {
        return __DIR__.'/Sepiroth/model.stub';
    }
    
    protected function getGridStub()
    {
        return __DIR__.'/Sepiroth/modelgrid.stub';
    }
    
    protected function getFormStub()
    {
        return __DIR__.'/Sepiroth/modelform.stub';
    }
    protected function getControllerStub()
    {
        return __DIR__.'/Sepiroth/controller.stub';
    }
    protected function alreadyExists($rawName)
    {
        $name = $this->parseName($rawName);
        $this->info('checking if exists');
        $this->info($name);
        return $this->files->exists($this->getPath($name));
    }
    protected function makeDirectory($path)
    {
        
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);   
        }
    }
    
    protected function getDefaultNamespace($rootNamespace)
    {
        $model = $this->argument('name');
        return $rootNamespace . '\Model';
    }

    protected function doBuildClass($name,$type)
    {
        if($type=='')
            $stub = $this->files->get($this->getStub());
        if($type=='grid')
            $stub = $this->files->get($this->getGridStub());
        if($type=='form')
            $stub = $this->files->get($this->getFormStub());
        

        $stub = $this->replaceDummyTable($stub, $name);
        $stub = $this->replaceDummyRoute($stub, $name);

        return $this->replaceNamespace($stub, $name)
                    ->replaceClass($stub, $name);
                    
    }

    protected function replaceDummyTable($stub, $name)
    {
        $table = strtolower($this->getNameInput());

        return str_replace('DummyTableName', $table, $stub);
    }
    protected function replaceDummyRoute($stub, $name)
    {
        $route = strtolower($this->getNameInput());

        return str_replace('DummyRoute', $route, $stub);
    }
    protected function replaceDummyModel($stub, $model)
    {
        return str_replace('DummyModel', $model, $stub);
    }
    protected function replaceDummyGrid($stub, $grid)
    {
        return str_replace('DummyGrid', $grid, $stub);
    }
    protected function replaceDummyForm($stub, $form)
    {
        return str_replace('DummyForm', $form, $stub);
    }
}
